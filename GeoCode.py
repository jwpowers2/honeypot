import geoip2.database


class GeoCode(object):

    def __init__(self,geofile1,geofile2):

        self.city_reader = geoip2.database.Reader(geofile1)
        self.asn_reader = geoip2.database.Reader(geofile2)

    def geocode(self,ip):
        asn_response = self.asn_reader.asn(ip)
        city_response = self.city_reader.city(ip)
        self.country = city_response.country.name
        self.city = city_response.city.name
        self.latitude = city_response.location.latitude
        self.longitude = city_response.location.longitude
        self.asn = asn_response.autonomous_system_organization
        return self


