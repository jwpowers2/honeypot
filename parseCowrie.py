import os,shutil,time
import requests
import sqlite3
import json
from getIp import get_ip
from GeoCode import GeoCode

# python 2


class parseCowrie(object):

    def __init__(self,file_location):

        self.conn = sqlite3.connect(file_location)   
        self.geocoder = GeoCode('GeoLite2-City.mmdb', 'GeoLite2-ASN.mmdb')

    def parseDB(self):
   
        c = self.conn.cursor()
        query = """SELECT ip, count(ip) 
                   FROM sessions WHERE datetime(starttime)
                    >datetime('now', '-1 day') 
                    group by ip"""
        try:

            c.execute(query)
            log_list = []
            for x in c.fetchall():
                try:
                    self.geocoder.geocode(x[0])
                    logOb = {}
                    logOb['src_ip'] = x[0]
                    logOb['count'] = x[1]
                    logOb['country'] = self.geocoder.country
                    logOb['city'] = self.geocoder.city
                    logOb['latitude'] = self.geocoder.latitude
                    logOb['longitude'] = self.geocoder.longitude
                    logOb['asn'] = self.geocoder.asn
                    log_list.append(logOb)                            

                except:
                    continue

            self.geocoder.geocode('209.97.153.184')
            protocol_output = {
                               'device':'nyc3',
                               'logs':log_list, 
                               'type':'ssh',
                               'device_ip_address':'167.71.88.68',
                               'country':self.geocoder.country,
                               'city':self.geocoder.city,
                               'latitude':self.geocoder.latitude,
                               'longitude':self.geocoder.longitude
                              }
            return protocol_output

        except:

            return False

    def get_stats(self):

        # get stats 
        output = {}
        output['device'] = 'nyc3'
        output['type'] = 'ssh'
        c = self.conn.cursor()
        try:
            day_count = """SELECT count(*) FROM sessions WHERE datetime(starttime)>datetime('now', '-1 day')"""
            c.execute(day_count)
            count_24_arr = c.fetchall()
            count_24 = count_24_arr[0][0]
            output['day_count'] = count_24
        except:
            pass
        try:
            previous_day_count = """SELECT count(*) 
                                FROM sessions WHERE datetime(starttime)>datetime('now', '-2 day') 
                                AND datetime(starttime)<datetime('now','-1 day')"""

            c.execute(previous_day_count)
            count_24_p_arr = c.fetchall()
            count_24_p = count_24_p_arr[0][0]
            output['previous_day_count'] = count_24_p
        except:
            pass
        try:
            max_query = """SELECT ip, count(ip) AS numip FROM sessions WHERE datetime(starttime)>datetime('now', '-1 day') GROUP BY ip ORDER BY numip DESC"""
            c.execute(max_query)
            max_arr = c.fetchall()
            #max_arr 0 0  is an array  ip, count
            max_ob = {}
            max_ob['ip'] = max_arr[0][0]
            max_ob['count'] = max_arr[0][1]
            output['maximum'] = max_ob
        except:
            pass
        return output

    def trim_sessions(self):

        c = self.conn.cursor()
        
        try:
            trim_query = """DELETE FROM sessions WHERE datetime(starttime)<datetime('now','-1 day')"""
            c.execute(trim_query)
            c.execute("VACUUM")
        except:
            pass

    def trim_auth(self):

        c = self.conn.cursor()
        
        try:
            trim_query = """DELETE FROM auth WHERE datetime(timestamp)<datetime('now','-1 day')"""
            c.execute(trim_query)
            c.execute("VACUUM")
        except:
            pass


    def close_conn(self):

        self.conn.close()


class removeLogs(object):

    def remove_logs(self, logPath):

        for the_file in os.listdir(logPath):
            file_path = os.path.join(logPath, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                #elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                #print(e)
                pass


# remove old logs
'''
remove_logs = removeLogs()
try:
    remove_logs.remove_logs('/home/cowrie/cowrie/var/log/cowrie')
except:
    pass
'''

# create parser object
p = parseCowrie('/home/cowrie/cowrie/cowrie.db')

# parse for bot data
bot_data = p.parseDB()
print bot_data
if (bot_data):
   
    data_to_send = json.dumps(bot_data)
     
    #SendData().send_logs('http://spottedbot.com:4000/api/data/logs',json.dumps(bot_data))
    requests.post('http://68.183.119.96:4000/api/test',json=bot_data)

#stats_data = p.get_stats()
#if (stats_data):
    #print str(stats_data)
    # there is something wrong with stats_data, it will not parse?
    #SendData().send_stats('http://206.189.183.239:4000/api/data/stats',json.dumps(stats_data))

#p.trim_sessions()
#p.trim_auth()
p.close_conn()

