import sqlite3
import json
import requests
from getIp import get_ip
from GeoCode import GeoCode

# this is a python3 program, run it with python3

class parseDionaea(object):

    def __init__(self,file_location):

        self.conn = sqlite3.connect(file_location)   
        self.geocoder = GeoCode('GeoLite2-City.mmdb', 'GeoLite2-ASN.mmdb')
        
    def parseDB(self,protocol):
   
        c = self.conn.cursor()
        query = """SELECT remote_host, count(remote_host) 
                   FROM connections WHERE datetime(connection_timestamp, 'unixepoch')
                    >datetime('now', '-1 day') AND connection_protocol='{}' 
                    group by remote_host""".format(protocol)
        try:

            c.execute(query)
            log_list = []
            for x in c.fetchall():
                try:
                    self.geocoder.geocode(x[0])
                    logOb = {}
                    logOb['src_ip'] = x[0]
                    logOb['count'] = x[1]
                    logOb['country'] = self.geocoder.country
                    logOb['city'] = self.geocoder.city
                    logOb['latitude'] = self.geocoder.latitude
                    logOb['longitude'] = self.geocoder.longitude
                    logOb['asn'] = self.geocoder.asn
                    log_list.append(logOb) 

                except Exception as e:
                    continue
            self.geocoder.geocode('209.97.153.184')
	    protocol_output = {}
	    protocol_output['device'] = 'nyc3'
	    protocol_output['logs'] = log_list
	    protocol_output['type'] = protocol
	    protocol_output['device_ip_address'] = '167.71.88.68'
	    protocol_output['country'] = self.geocoder.country
	    protocol_output['city'] = self.geocoder.city
	    protocol_output['latitude'] = self.geocoder.latitude
	    protocol_output['longitude'] = self.geocoder.longitude
 
            return protocol_output

        except Exception as e:
            pass

    def get_stats(self,protocol):

        # get stats 
        output = {}
        output['device'] = 'nyc3'
        output['type'] = protocol
        c = self.conn.cursor()
        try:
            day_count = """SELECT count(*) FROM connections WHERE datetime(connection_timestamp, 'unixepoch')>datetime('now', '-1 day') AND connection_protocol='{}'""".format(protocol)
            c.execute(day_count)
            count_24_arr = c.fetchall()
            count_24 = count_24_arr[0][0]
            output['day_count'] = count_24
        except:
            pass
        try:
            previous_day_count = """SELECT count(*) 
                                FROM connections WHERE datetime(connection_timestamp, 'unixepoch')>datetime('now', '-2 day') 
                                AND datetime(connection_timestamp, 'unixepoch')<datetime('now','-1 day') AND connection_protocol='{}'""".format(protocol)

            c.execute(previous_day_count)
            count_24_p_arr = c.fetchall()
            count_24_p = count_24_p_arr[0][0]
            output['previous_day_count'] = count_24_p
        except:
            pass
        try:
            max_query = """SELECT remote_host, count(remote_host) AS rh FROM connections WHERE datetime(connection_timestamp, 'unixepoch')>datetime('now', '-1 day') AND connection_protocol='{}' GROUP BY remote_host ORDER BY rh DESC""".format(protocol)
            c.execute(max_query)
            max_arr = c.fetchall()
            #max_arr 0 0  is an array  ip, count
            max_ob = {}
            max_ob['ip'] = max_arr[0][0]
            max_ob['count'] = max_arr[0][1]
            output['maximum'] = max_ob
        except:
            pass
        return output

    def close_conn(self):

        self.conn.close()

    def trimDB(sef):

        pass

class removeLogs(object):

    def remove_logs(self, logPath):

        with open(logPath, 'w') as log:

            pass
'''
remove_logs = removeLogs()
try:
    remove_logs.remove_logs('/opt/dionaea/var/log/dionaea/dionaea.log')
except:
    pass
'''
p = parseDionaea('/opt/dionaea/var/lib/dionaea/dionaea.sqlite')
protocol_list = ['smbd',
                 'httpd',
                 'pptpd',
                 'mysqld',
                 'mqttd',
                 'tftpd',
                 'upnpd',
                 'SipSession',
                 'mongodbd',
                 'ftpd',
                 'mssqld']
for prot in protocol_list:

    # send log data
    bot_data = p.parseDB(prot)
    if (bot_data):
        x = requests.post('http://spottedbot.com/api/test', json=bot_data)
    '''
    stats_data = p.get_stats(prot)
    if (stats_data):
        #print json.dumps(stats_data)  
        SendData().send_stats('http://68.183.119.96:4000/api/test', json.dumps(stats_data))
    '''
